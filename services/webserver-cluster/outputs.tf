output "clb_dns_name" {
  value       = aws_elb.demo_elb.dns_name
  description = "The domain name of the load balancer"
}

output "asg_name" {
  value       = aws_autoscaling_group.demo_asg.name
  description = "the name of the Auto Scaling Group."
}
