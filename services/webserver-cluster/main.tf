
terraform {
  backend "s3" {
    bucket = "tform-state"
    key    = "stage/services/webvserver/terraform.tfstate"
    region = "ap-southeast-2"

    dynamodb_table = "terraform_locks"
    encrypt        = true
  }
}


data "aws_availability_zones" "all" {

}

data "terraform_remote_state" "s3" {
  backend = "s3"
  config = {
    bucket = "tform-state"
    key    = "global/s3/terraform.tfstate"
    region = "ap-southeast-2"
  }
}


resource "aws_launch_configuration" "demo" {
  image_id        = "ami-07e22925f7bf77a0c"
  instance_type   = var.instance_type
  security_groups = [aws_security_group.instance.id]

  # user_data = file("../../../scripts/simple.sh")
  user_data = <<-EOF
              #!/bin/bash
              dynamodb_table_name="${data.terraform_remote_state.s3.outputs.dynamodb_table_name}"
              echo "Hello, World. Terraform lock table is at '$dynamodb_table_name'." >> index.html
              nohup busybox httpd -f -p "${var.webserver_port}" &
              EOF
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "demo_asg" {
  launch_configuration = aws_launch_configuration.demo.id
  availability_zones   = data.aws_availability_zones.all.names

  min_size = var.min_size
  max_size = var.max_size

  load_balancers    = [aws_elb.demo_elb.name]
  health_check_type = "ELB"

  tag {
    key                 = "Name"
    value               = var.cluster_name
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = {
      for key, value in var.custom_tags :
      key => upper(value)
      if key != "Name"
    }

    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}

resource "aws_autoscaling_schedule" "scale_out_business_hours" {
  count = var.enable_scaling ? 1 : 0

  scheduled_action_name = "scale-out-during-business-hours"
  min_size              = 2
  max_size              = 5
  desired_capacity      = 5
  recurrence            = "0 9 * * 1-5"

  autoscaling_group_name = aws_autoscaling_group.demo_asg.name
}
resource "aws_autoscaling_schedule" "scale_in_at_night" {
  count = var.enable_scaling ? 1 : 0

  scheduled_action_name = "scale-in-after-business-hours"
  min_size              = 2
  max_size              = 5
  desired_capacity      = 2
  recurrence            = "0 17 * * 1-5"

  autoscaling_group_name = aws_autoscaling_group.demo_asg.name
}


resource "aws_security_group" "instance" {
  name = "${var.cluster_name}-sg"

  ingress {
    from_port   = var.webserver_port
    to_port     = var.webserver_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "demo_elb" {
  name               = "${var.cluster_name}-asg"
  security_groups    = [aws_security_group.elb.id]
  availability_zones = data.aws_availability_zones.all.names

  health_check {
    target              = "HTTP:${var.webserver_port}/"
    interval            = 30
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  listener {
    lb_port           = var.elb_port
    lb_protocol       = "http"
    instance_port     = var.webserver_port
    instance_protocol = "http"
  }
}

resource "aws_security_group" "elb" {
  name = "${var.cluster_name}-elb"

  # Allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all inbound
  ingress {
    from_port   = var.elb_port
    to_port     = var.elb_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

