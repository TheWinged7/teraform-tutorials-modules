variable "webserver_port" {
  description = "default port configuration of simple demo webserver."
  type        = number
  default     = 8080
}

variable "elb_port" {
  description = "default port configuration of simple demo elb."
  type        = number
  default     = 80
}

variable "cluster_name" {
  description = "The name to use for all the cluster resources"
  type        = string
}

variable "instance_type" {
  description = "The type of EC2 Instances to run (e.g. t2.micro)"
  type        = string
  default     = "t2.micro"
}
variable "min_size" {
  description = "The minimum number of EC2 Instances in the ASG"
  type        = number
  default     = 2
}
variable "max_size" {
  description = "The maximum number of EC2 Instances in the ASG"
  type        = number
  default     = 5
}

variable "custom_tags" {
  description = "Custom tags to set on the Instances in the ASG"
  type        = map(string)
  default     = {}
}

variable "enable_scaling" {
  description = "Enable autoscaling for EC2 instances in the ASG"
  type        = bool
  default     = false
}
